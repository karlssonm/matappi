//
//  SearchHandler.h
//  MatAppi
//
//  Created by Mattias Karlsson on 2015-03-20.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FoodTableView.h"

@interface SearchHandler : NSObject

+(NSMutableArray*) getFood;
//+(NSDictionary*) getNutrient:(NSNumber*) number andDictionary:(NSDictionary*) dataSource;

@end
