//
//  SearchHandler.m
//  MatAppi
//
//  Created by Mattias Karlsson on 2015-03-20.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import "SearchHandler.h"
@interface SearchHandler ()
@end

@implementation SearchHandler


+(NSMutableArray *)getFood {
    NSMutableArray *foods = [[NSMutableArray alloc] init];
    NSString *searchString = [NSString stringWithFormat: @"http://www.matapi.se/foodstuff?query="];
    NSURL *url = [NSURL URLWithString:searchString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *task =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                   if (error) {
                       NSLog(@"error in response %@",error);
                       return;
                   }
                   
                   NSError *parsingError = nil;
                   NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data
                                         options:kNilOptions error:&parsingError];
                   if(!parsingError) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           if (root.count>0) {
                               for (NSData *item in root) {
                                   [foods addObject:item];
                               }
                           }
                       });
                       
                       
                   }
                   else {
                       NSLog(@"couldn't parse parsinerror: %@", parsingError);
                    }
               }];
    [task resume];
    return foods;
}

//+(NSDictionary*)getNutrient :(NSNumber*) number andDictionary:(NSDictionary*)dataSource {
   // __block NSDictionary *nutrients = [[NSDictionary alloc] init];
     //   return nutrients;
//}
@end

