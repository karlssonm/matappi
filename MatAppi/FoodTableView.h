//
//  FoodTableView.h
//  MatAppi
//
//  Created by Mattias Karlsson on 2015-03-17.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NutrientsView.h"
#import "SearchHandler.h"
#import "SearchTableViewCell.h"

@interface FoodTableView : UITableViewController <UISearchBarDelegate>//uisearchcontrollerdelegate
@property (nonatomic) NSArray *tableFoods;
@property (nonatomic) NSDictionary *nutValues;
@property (nonatomic) NSArray *searchResults;
-(void)reloadTableData;
@end
