//
//  FoodTableView.m
//  MatAppi
//
//  Created by Mattias Karlsson on 2015-03-17.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import "FoodTableView.h"

@interface FoodTableView ()
@property (strong, nonatomic) IBOutlet UITableView *table;
@end

@implementation FoodTableView

- (void)viewDidLoad {
    [super viewDidLoad];
    if (self.tableFoods.count == 0) {
        [self reloadTableData];
    }
}
-(void)reloadTableData {
    [self.table reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (tableView == self.tableView) {
        return self.tableFoods.count;
    }else {
        return self.searchResults.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray * showArray;
    if (tableView== self.tableView) {
        showArray = self.tableFoods;
        
    } else {
        showArray = self.searchResults;
    }
    SearchTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"MyCell" forIndexPath:indexPath];
    cell.cellIdentifier = [showArray objectAtIndex:indexPath.row][@"number"];
    cell.textLabel.text= [showArray objectAtIndex:indexPath.row][@"name"];
    return cell;
}
/*- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.searchSelect = indexPath;
    //[self performSegueWithIdentifier:@"FilteredNutrient" sender:self];
    //NSLog(@"%@",[SearchHandler getNutrient:[self.tableFoods objectAtIndex:indexPath.row][@"number"]]);
    //self.nutValues =
}
*/
/*- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!(tableView == self.tableView)){
        [self performSegueWithIdentifier:@"FilteredNutrient" sender:self];
    }
}
*/
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF['name'] CONTAINS [c]%@",searchText ];
    self.searchResults = [self.tableFoods filteredArrayUsingPredicate:predicate];
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
*/


#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    SearchTableViewCell *cell = sender;
    NutrientsView *nutrientView = [segue destinationViewController];
    if([segue.identifier isEqualToString:@"Nutrient"]) {
        NSString *search = [NSString stringWithFormat:@"%@",cell.cellIdentifier];
        
        NSString *searchString = [NSString stringWithFormat:@"http://matapi.se/foodstuff/%@", search];
        NSURL *url = [NSURL URLWithString:searchString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *task =
        [session dataTaskWithRequest:request
                   completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                       if (error) {
                           NSLog(@"error in response %@",error);
                           return;
                       }
                       
                       NSError *parsingError = nil;
                       NSDictionary *root = [NSJSONSerialization JSONObjectWithData:data
                                                options:kNilOptions error:&parsingError];
                       if(!parsingError) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               if (root.count>0) {
                                   
                                   nutrientView.nutrients = root[@"nutrientValues"];
                                   nutrientView.title = root[@"name"];
                                   [nutrientView populateLabels];
                               }
                           });
                           
                       }
                       else {
                           NSLog(@"couldn't parse parsinerror: %@", parsingError);
                       }
                   }];
        [task resume];

    }
}
@end
