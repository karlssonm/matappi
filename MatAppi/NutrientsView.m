//
//  NutrientsView.m
//  MatAppi
//
//  Created by Mattias Karlsson on 2015-03-22.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import "NutrientsView.h"

@interface NutrientsView ()
@property (weak, nonatomic) IBOutlet UILabel *energyLabel;
@property (weak, nonatomic) IBOutlet UILabel *proteinLabel;
@property (weak, nonatomic) IBOutlet UILabel *carbLabel;
@property (weak, nonatomic) IBOutlet UILabel *fatLabel;
@property (weak, nonatomic) IBOutlet UILabel *saltLabel;
@property (weak, nonatomic) IBOutlet UILabel *vitaminCLabel;
@property (weak, nonatomic) IBOutlet UILabel *cholesterolLabel;
@property (weak, nonatomic) IBOutlet UILabel *healthValue;
@property (nonatomic) float rdiPercentage;

@end

@implementation NutrientsView

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)populateLabels {
    self.fatLabel.text = [self Labeltext:@"fat"];
    self.proteinLabel.text= [self Labeltext:@"protein"];
    self.carbLabel.text= [self Labeltext:@"carbohydrates"];
    self.energyLabel.text= [self Labeltext:@"energyKj"];
    self.saltLabel.text= [self Labeltext:@"salt"];
    self.vitaminCLabel.text= [self Labeltext:@"vitaminC"];
    self.cholesterolLabel.text= [self Labeltext:@"cholesterol"];
    self.rdiPercentage = [[self.nutrients objectForKey:@"energyKcal"] floatValue]/29;
    self.healthValue.text = [NSString stringWithFormat:@"%.02f procent",self.rdiPercentage];
}
-(NSString*)Labeltext:(NSString*)key {
    return [NSString stringWithFormat:@"%@" ,[self.nutrients objectForKey:key]];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
