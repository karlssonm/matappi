//
//  ViewController.m
//  MatAppi
//
//  Created by Mattias Karlsson on 2015-03-11.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (nonatomic) UIDynamicAnimator *animator;
@property (weak, nonatomic) IBOutlet UIImageView *carrot;
@property (weak, nonatomic) IBOutlet UIImageView *carrot2;
@property (weak, nonatomic) IBOutlet UIImageView *carrot3;
@property (weak, nonatomic) IBOutlet UIImageView *carrot4;
@property (weak, nonatomic) IBOutlet UIButton *goButton;
@property (nonatomic) UIGravityBehavior *gravity;
@property (nonatomic) UICollisionBehavior *collision;
@property (weak, nonatomic) IBOutlet UIButton *hiddenButton;
@end

@implementation ViewController
- (IBAction)GoButton:(id)sender {
    [UIView animateWithDuration:1 animations:^{
        self.goButton.center = CGPointMake(self.goButton.center.x, self.goButton.center.y+100);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1 delay:0.3 options:kNilOptions animations:^{
            self.goButton.center = CGPointMake(self.goButton.center.x+300, self.goButton.center.y);
        } completion:^(BOOL finished) {
            [self performSegueWithIdentifier:@"toTable" sender:self];
        }];
    }];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.hiddenButton.hidden = YES;
    NSArray * carrots = @[self.carrot, self.carrot2,self.carrot3,self.carrot4];
    
    self.animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    
    self.gravity = [[UIGravityBehavior alloc] initWithItems:carrots];
    [self.animator addBehavior:self.gravity];
    self.collision = [[UICollisionBehavior alloc] initWithItems:carrots];
    self.collision.translatesReferenceBoundsIntoBoundary = YES;
    [self.animator addBehavior:self.collision];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"toTable"]) {
        FoodTableView *tableView = [segue destinationViewController];
        NSString *searchString = [NSString stringWithFormat: @"http://www.matapi.se/foodstuff?query="];
        NSURL *url = [NSURL URLWithString:searchString];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        NSURLSession *session = [NSURLSession sharedSession];
        
        NSURLSessionDataTask *task =
        [session dataTaskWithRequest:request
                   completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                       if (error) {
                           NSLog(@"error in response %@",error);
                           return;
                       }
                       
                       NSError *parsingError = nil;
                       NSArray *root = [NSJSONSerialization JSONObjectWithData:data
                                                                            options:kNilOptions error:&parsingError];
                       if(!parsingError) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               if (root.count>0) {
                                   tableView.tableFoods = root;
                                   [tableView reloadTableData];
                               }
                           });
                       }
                       else {
                           NSLog(@"couldn't parse parsinerror: %@", parsingError);
                       }
                   }];
        [task resume];
    }
}

@end
