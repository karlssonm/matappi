//
//  ViewController.h
//  MatAppi
//
//  Created by Mattias Karlsson on 2015-03-11.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchHandler.h"
#import "FoodTableView.h"

@interface ViewController : UIViewController


@end

