//
//  NutrientsView.h
//  MatAppi
//
//  Created by Mattias Karlsson on 2015-03-22.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NutrientsView : UIViewController
@property (nonatomic) NSDictionary *nutrients;
-(void)populateLabels;
@end
