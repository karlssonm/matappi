//
//  SearchTableViewCell.h
//  MatAppi
//
//  Created by Mattias Karlsson on 2015-04-24.
//  Copyright (c) 2015 Mattias Karlsson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell
@property (nonatomic) NSNumber *cellIdentifier;
@end
